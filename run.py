# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from app import app

import click

@click.command()
@click.option('--ip', type=str, required=True, help='Adress IP of host')
def main(ip):
    app.run(host=ip, debug=False, use_reloader=False)


if __name__ == '__main__':
    main()
