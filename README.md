# Web Deep Learning Vision Demos

![put vision](https://putvision.github.io/assets/images/cropped-LOGO_PUT_VISION_LAB_MAIN.webp)

## Aim

The goal of the project is to develop a tool that easly can visualize the effect of inference neural networks. It uses Flask, OpenCV, and ONNXRuntime to perform it on embedded Jetson Xavier NX. All demos are run on the TensorRT backend and visualized on the browser.

## Usage

#### **Requrements**

Simply, install missing libraries using pip installer:

```bash
pip3 install -r requirements.txt
```

#### **Run**

One you need is to precise the IP address of your machine (you can check it using `ip a`).

```bash
python3 run.py --ip <HOST_IP>
```


## Models

#### **Classification**

* [Resnet family](https://github.com/onnx/models/tree/main/vision/classification/resnet) (resnet18, resnet34m resnet50)
* [EfficientNet-Lite4](https://github.com/onnx/models/tree/main/vision/classification/efficientnet-lite4)

#### **Object detection**

* [YOLOv5 family](https://github.com/ultralytics/yolov5) (yolov5-nano, yolov5-medium)
* [YOLOv7 family](https://github.com/WongKinYiu/yolov7) (yolov7, yolov7-tiny)

#### **Segmentation**

* [FCN ResNet-50](https://github.com/onnx/models/tree/main/vision/object_detection_segmentation/fcn)
* [Fastseg family](https://github.com/ekzhang/fastseg) (MobileV3Small, MobileV3Large)

#### **Keypoints estimation**

* [PoseNet family](https://github.com/microsoft/human-pose-estimation.pytorch) (Resnet50, Resnet152)

#### **Mono Camera Depth estimation**

* [FastDepth](https://github.com/dwofk/fast-depth)

#### **Style transfer**

* [Fast Neural Style Transfer family](https://github.com/onnx/models/tree/main/vision/style_transfer/fast_neural_style) (mosaic, candy, rain princess, udnie)

## Authors

* [Bartosz Ptak](https://github.com/bartoszptak)

## Screenshots

![home](README_files/home.png)

![demo](README_files/demo.png)