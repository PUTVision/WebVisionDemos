import cv2
from importlib import import_module
from matplotlib.colors import hsv_to_rgb
import numpy as np
from typing import List
from PIL import Image
import onnxruntime as ort
import time
import collections

class FPS:
    def __init__(self,avarageof=50):
        self.frametimestamps = collections.deque(maxlen=avarageof)

    def __call__(self, update_time):
        self.frametimestamps.append(update_time)

    def __str__(self):
        if(len(self.frametimestamps) > 1):
            return f'{len(self.frametimestamps)/(np.sum(self.frametimestamps)):.2f}'
        else:
            return '0.0'


class SuperModel:
    def __init__(self):
        self._model = None
        self._kind = None
        self.COLORS = np.random.uniform(0, 255, size=(100, 3))

        self.options = ort.SessionOptions()
        self.options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL

        self.providers = [
            ('TensorrtExecutionProvider', {
                'trt_fp16_enable': True,
                'trt_engine_cache_enable': True,
            }),
            'CUDAExecutionProvider',
            'CPUExecutionProvider'
        ]

        self.fps = FPS()

    @staticmethod
    def get_palette(num_classes):
    # prepare and return palette
        palette = [0] * num_classes * 3

        for hue in range(num_classes):
            if hue == 0: # Background color
                colors = (0, 0, 0)
            else:
                colors = hsv_to_rgb((hue / num_classes, 0.75, 0.75))

            for i in range(3):
                palette[hue * 3 + i] = int(colors[i] * 255)

        return palette

    def update(self, data: dict):
        if self._model is not None:
            self._model = None
            self.fps = FPS()

        module_path, class_name = data['class'].rsplit('.', 1)
        module = import_module(module_path)

        self._kind = data['kind']
        self._model = getattr(module, class_name)(data=data['data'], options=self.options, providers=self.providers)

    def predict_and_visualize(self, frame: np.array):
        if self._model is None:
            return frame

        start_time = time.perf_counter()
        outs = self._model.predict(frame.copy())
        stop_time = time.perf_counter()

        self.fps(stop_time-start_time)

        frame = self.visualize(frame, outs)

        return frame


    def visualize(self, frame: np.array, outs: List):
        if self._kind == 'object_detection':

            classes = {}

            for box, conf, cl, name in zip(*outs):
                x1, y1, x2, y2 = box

                if name in classes:
                    classes[name] += 1
                else:
                    classes[name] = 1

                color = self.COLORS[cl]

                cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), color, 2)
                conf *= 100
                cv2.putText(frame, f'{name} ({conf:.1f}%)', (int(x1), int(y1)-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)

        
            legend = np.zeros((frame.shape[0], 250, 3))
            for i, (key, val) in enumerate(sorted(classes.items(), key=lambda kv: kv[1], reverse=True)[:20]):
                cv2.putText(legend, f'{val}-{key}', (10, i*20+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)

            frame = np.hstack([frame, legend])

        elif self._kind == 'classification':
            legend = np.zeros((frame.shape[0], 250, 3))
            for i, (name, prob) in enumerate(outs):
                prob *= 100
                cv2.putText(legend, f'{prob:.1f}%-{name}', (10, i*20+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 1)

            frame = np.hstack([frame, legend])

        elif self._kind == 'depth_estimator':
            outs = np.transpose(outs, (1,2,0))
            outs = np.clip(outs, 0, 5)
            outs = outs / 5 * 255
            outs = np.uint8(outs)
            outs = cv2.resize(outs, frame.shape[:2][::-1])

            frame = cv2.applyColorMap(outs, cv2.COLORMAP_INFERNO)

        elif self._kind == 'keypoints_estimator':

            for (x,y) in outs:
                if x==-1 or y==-1:
                    continue

                cv2.circle(frame, (int(x), int(y)), 8, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)

            for pair in self._model.points_links:
                partA = pair[0]
                partB = pair[1]
                
                if outs[partA][0]==-1 or outs[partB][0]==-1:
                    continue

                cv2.line(frame, outs[partA], outs[partB], (0, 255, 144), 2)

        elif self._kind == 'segmentation':

            labels, confidence, classes, names = outs
            colors = self.get_palette(len(names))

            result_img = Image.fromarray(labels).convert('P', colors=colors)
            result_img.putpalette(colors)
            result_img = np.array(result_img.convert('RGB'))
            result_img = cv2.resize(result_img, frame.shape[:2][::-1])
            result_img = cv2.medianBlur(result_img, 5)

            frame = cv2.addWeighted(frame, 0.3, result_img, 0.7, 0)
            legend = np.zeros((frame.shape[0], 250, 3))

            for i, (col, cl) in enumerate(zip(np.array(colors).reshape(-1, 3), names)):
                col = col.tolist()
                cv2.putText(legend, f'{cl}', (10, i*20+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, col, 1)

            frame = np.hstack([frame, legend])


        elif self._kind == 'style_transfer':
            frame = cv2.resize(outs, frame.shape[:2][::-1])

        fps_label = np.zeros((25, frame.shape[1], 3))
        cv2.putText(fps_label, f'Mean FPS: {self.fps}', (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255), 1)

        frame = np.vstack([fps_label, frame])
  
        return frame