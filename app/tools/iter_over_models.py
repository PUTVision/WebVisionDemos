import yaml
from tqdm import tqdm
from importlib import import_module
import sys

sys.path.append('./')

def iter_over_models():
    with open("app/models.yaml", "r") as stream:
        models = yaml.safe_load(stream)['models']

    models = [k for m in models for k in models[m]['models']]
    for i, model in enumerate(models):
        print(f'{i+1}/{len(models)} - {model["name"]}')
        
        module_path, class_name = model['class'].rsplit('.', 1)
        module = import_module(module_path)

        m = getattr(module, class_name)(data=model['data'])

        del m


if __name__ == '__main__':
    iter_over_models()