# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

# Python modules
import os, logging 
import yaml
import cv2

# Flask modules
from flask               import render_template, request, url_for, redirect, flash, Response
from werkzeug.exceptions import HTTPException, NotFound
from jinja2              import TemplateNotFound

# App modules
from app        import app
from app.supermodel import SuperModel

import numpy as np

with open("app/models.yaml", "r") as stream:
    models = yaml.safe_load(stream)['models']

supermodel = SuperModel()

for i in range(10):
    camera = cv2.VideoCapture(i)
    if camera.isOpened():
        break

# App main route + generic routing
@app.route('/', defaults={'path': 'index.html'})
def index(path):
    return render_template( 'home/' + path , models=models)


@app.route('/demo/<model>', defaults={'path': 'demo.html'})
def demo(model, path):
    data = [k for m in models for k in models[m]['models'] if k['key']==model]
    supermodel.update(data=data[0])
    return render_template( 'home/' + path , data=data[0])

def gen_frames():  
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            new_frame = np.full((700, 1000, 3), 255, dtype=np.uint8)
            cv2.putText(new_frame, f'Fatal: NO ACCESS TO CAMERA!', (100, 350), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,0,255), 1)
            print('Fatal: NO ACCESS TO CAMERA!')
        else:
            try:
                new_frame = supermodel.predict_and_visualize(frame)
            except Exception as e:
                new_frame = np.full((700, 1000, 3), 255, dtype=np.uint8)
                print(e)
                cv2.putText(new_frame, f'{e}', (100, 350), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,0,255), 1)

        h = 700
        s = int(h/new_frame.shape[0]*new_frame.shape[1])
        new_frame = cv2.resize(new_frame, (s,h))

        ret, buffer = cv2.imencode('.jpg', new_frame)
        buffer = buffer.tobytes()
        yield (b'--frame\r\n'

                b'Content-Type: image/jpeg\r\n\r\n' + buffer + b'\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')
