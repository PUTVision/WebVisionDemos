#!/usr/bin/env python3

from typing import Tuple
import onnxruntime as ort
import cv2
import numpy as np

class Resnet:
    def __init__(self, data, options, providers):    
        self.session = ort.InferenceSession(data['model_path'], options, providers=providers)

        self.input_names = [inp.name for inp in self.session.get_inputs()]
        self.input_shape = self.session.get_inputs()[0].shape[2:4][::-1]

        self.classes = eval(open('./app/models/classification/imagenet1000_clsidx_to_labels.txt').read())

    @staticmethod
    def softmax(x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum(axis=0) # only difference

    def preprocess(self, frame: np.ndarray) -> np.ndarray:
        input_data = cv2.resize(frame, self.input_shape)
        input_data = cv2.cvtColor(input_data, cv2.COLOR_BGR2RGB)
        input_data = (input_data / 255.0).astype(np.float32)
        input_data = (input_data-np.array([0.485, 0.456, 0.406])) / np.array([0.229, 0.224, 0.225])
        input_data = np.transpose(input_data.astype(np.float32), (2, 0, 1))

        return np.expand_dims(input_data, 0)

    def predict(self, image):
        inp = self.preprocess(image.copy())
        outs = self.session.run(None, {self.input_names[0]: inp})

        outs = self.postprocess(outs[0][0], image.shape)

        return outs

    def postprocess(self, predictions: np.ndarray, org_shape: np.ndarray) -> np.ndarray:
        predictions = self.softmax(predictions)
        inds = np.argsort(predictions, axis=0)[-5:][::-1]

        z = [(self.classes[label], predictions[label]) for label in inds]
        return z