#!/usr/bin/env python3

from typing import Tuple
import onnxruntime as ort
import cv2
import numpy as np

class EfficientNet:
    def __init__(self, data, options, providers):    
        self.session = ort.InferenceSession(data['model_path'], options, providers=providers)

        self.input_names = [inp.name for inp in self.session.get_inputs()]
        self.input_shape = self.session.get_inputs()[0].shape[1:3][::-1]

        self.classes = eval(open('./app/models/classification/imagenet1000_clsidx_to_labels.txt').read())

    def preprocess(self, frame: np.ndarray) -> np.ndarray:
        input_data = self.resize_with_aspectratio(frame, self.input_shape[1], self.input_shape[0], inter_pol=cv2.INTER_LINEAR)
        input_data = self.center_crop(input_data, self.input_shape[1], self.input_shape[0])
        input_data = cv2.cvtColor(input_data, cv2.COLOR_BGR2RGB).astype(np.float32)
        input_data = (input_data-np.array([127.0, 127.0, 127.0])) / np.array([128.0, 128.0, 128.0])
        input_data = input_data.astype(np.float32)

        return np.expand_dims(input_data, 0)

    def predict(self, image):
        inp = self.preprocess(image.copy())
        outs = self.session.run(None, {self.input_names[0]: inp})

        outs = self.postprocess(outs[0][0], image.shape)

        return outs

    def postprocess(self, predictions: np.ndarray, org_shape: np.ndarray) -> np.ndarray:
        inds = np.argsort(predictions, axis=0)[-5:][::-1]
        return [(self.classes[label], predictions[label]) for label in inds]


    @staticmethod
    def resize_with_aspectratio(img, out_height, out_width, scale=87.5, inter_pol=cv2.INTER_LINEAR):
        height, width, _ = img.shape
        new_height = int(100. * out_height / scale)
        new_width = int(100. * out_width / scale)
        if height > width:
            w = new_width
            h = int(new_height * height / width)
        else:
            h = new_height
            w = int(new_width * width / height)
        img = cv2.resize(img, (w, h), interpolation=inter_pol)
        return img

    @staticmethod
    # crop the image around the center based on given height and width
    def center_crop(img, out_height, out_width):
        height, width, _ = img.shape
        left = int((width - out_width) / 2)
        right = int((width + out_width) / 2)
        top = int((height - out_height) / 2)
        bottom = int((height + out_height) / 2)
        img = img[top:bottom, left:right]
        return img