#!/usr/bin/env python3

from typing import Tuple
import onnxruntime as ort
import cv2
import numpy as np


class YOLOv5:
    def __init__(self, data, options, providers):
        self.session = ort.InferenceSession(data['model_path'], options, providers=providers)

        self.input_names = [inp.name for inp in self.session.get_inputs()]
        self.input_shape = self.session.get_inputs()[0].shape[2:4][::-1]

        self.score_threshold = data['conf_tresh']
        self.iou_threshold = data['iou_tresh']

        self.classes = eval(open('./app/models/object_detection/ms_coco_classnames.txt').read())


    def preprocess(self, frame: np.ndarray) -> np.ndarray:
        input_data = cv2.resize(frame, self.input_shape)
        input_data = cv2.cvtColor(input_data, cv2.COLOR_BGR2RGB)
        input_data = (input_data / 255.0).astype(np.float32)
        input_data = np.transpose(input_data, (2, 0, 1))

        return np.expand_dims(input_data, 0)

    def predict(self, image):
        inp = self.preprocess(image.copy())
        outs = self.session.run(None, {self.input_names[0]: inp})

        outs = self.postprocess(outs[0], image.shape)

        return outs

    def postprocess(self, predictions: np.ndarray, org_shape: np.ndarray) -> np.ndarray:
        outputs_filtered = np.array(list(filter(lambda x: x[4] >= self.score_threshold, predictions[0])))

        if len(outputs_filtered.shape) < 2:
            return [], [], [], []


        # Convert x_center,y_center,w,h to x1,y1,x2,y2
        outputs_x1y1x2y2 = self.xywh2xyxy(outputs_filtered)

        outputs_nms = self.non_max_suppression_fast(outputs_x1y1x2y2, self.iou_threshold)

        outputs_nms[:, :4] = np.multiply(outputs_nms[:, :4] / self.input_shape[0],
                                         [org_shape[1], org_shape[0], 
                                          org_shape[1], org_shape[0]])

        boxes = outputs_nms[:, :4]
        conf = outputs_nms[:, 4]
        classes = np.argmax(outputs_nms[:, 5:], axis=1)
        names = [self.classes[c+1] for c in classes]

        return boxes, conf, classes, names

    @staticmethod
    def xywh2xyxy(x):
        y = np.copy(x)
        y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
        y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
        y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
        y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
        return y

    @staticmethod
    def non_max_suppression_fast(boxes: np.ndarray, iou_threshold: float) -> np.ndarray:

        # initialize the list of picked indexes
        pick = []
        # grab the coordinates of the bounding boxes
        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]
        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = np.argsort(y2)
        # keep looping while some indexes still remain in the indexes
        # list
        while len(idxs) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)
            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])
            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)
            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]
            # delete all indexes from the index list that have
            idxs = np.delete(idxs, np.concatenate(([last],
                                                np.where(overlap > iou_threshold)[0])))

        return boxes[pick]