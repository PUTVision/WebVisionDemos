#!/usr/bin/env python3

from typing import Tuple
import onnxruntime as ort
import cv2
import numpy as np

class PoseResnet:
    def __init__(self, data, options, providers):    
        self.session = ort.InferenceSession(data['model_path'], options, providers=providers)

        self.input_names = [inp.name for inp in self.session.get_inputs()]
        self.input_shape = self.session.get_inputs()[0].shape[2:4][::-1]

        self.points_links = [ [0,1],[0,2],[1,3],[2,4],[0,5],[0,6],[6,8],[8,10],[5,7],[7,9],[5,11],[6,12],[11,13],[13,15],[12,14],[14,16],[12, 11], [6, 5] ]
        self.points_number = 17
        self.threshold = 0.5

    @staticmethod
    def softmax(x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum(axis=0) # only difference

    def preprocess(self, frame: np.ndarray) -> np.ndarray:
        input_data = cv2.resize(frame, self.input_shape)
        # input_data = cv2.cvtColor(input_data, cv2.COLOR_BGR2RGB)
        input_data = (input_data / 255.0).astype(np.float32)
        input_data = np.transpose(input_data.astype(np.float32), (2, 0, 1))

        return np.expand_dims(input_data, 0)

    def predict(self, image):
        inp = self.preprocess(image.copy())
        outs = self.session.run(None, {self.input_names[0]: inp})

        outs = self.postprocess(outs[0][0], image.shape)

        return outs

    def postprocess(self, predictions: np.ndarray, org_shape: np.ndarray) -> np.ndarray:
        H = predictions.shape[1]
        W = predictions.shape[2]

        points = []
        for i in range(self.points_number):
            probMap = predictions[i, :, :]
            
            minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

            x = (org_shape[1] * point[0]) / W
            y = (org_shape[0] * point[1]) / H

            if prob > self.threshold:
                points.append((int(x), int(y)))
            else:
                points.append((-1, -1))

        return points