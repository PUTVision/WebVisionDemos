#!/usr/bin/env python3

from typing import Tuple
import onnxruntime as ort
import cv2
import numpy as np

class FastDepth:
    def __init__(self, data, options, providers):
        self.session = ort.InferenceSession(data['model_path'], options, providers=providers)

        self.input_names = [inp.name for inp in self.session.get_inputs()]
        self.input_shape = self.session.get_inputs()[0].shape[2:4][::-1]

    def preprocess(self, frame: np.ndarray) -> np.ndarray:
        input_data = cv2.resize(frame, self.input_shape)
        input_data = cv2.cvtColor(input_data, cv2.COLOR_BGR2RGB)
        input_data = (input_data / 255.0).astype(np.float32)
        input_data = np.transpose(input_data.astype(np.float32), (2, 0, 1))

        return np.expand_dims(input_data, 0)

    def predict(self, image):
        inp = self.preprocess(image.copy())
        outs = self.session.run(None, {self.input_names[0]: inp})

        outs = self.postprocess(outs[0][0], image.shape)

        return outs

    def postprocess(self, predictions: np.ndarray, org_shape: np.ndarray) -> np.ndarray:
        return predictions